Source: prettytable
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>,
Build-Depends: debhelper-compat (= 13),
               pybuild-plugin-pyproject,
               python3-all (>= 3.1.3-3),
               python3-hatch-vcs,
               python3-hatchling,
               python3-pytest <!nocheck>,
               python3-pytest-lazy-fixture <!nocheck>,
               python3-setuptools,
               python3-setuptools-scm,
               python3-wcwidth,
Standards-Version: 4.6.2.0
Homepage: https://github.com/jazzband/prettytable/
Vcs-Git: https://salsa.debian.org/python-team/packages/prettytable.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/prettytable
Testsuite: autopkgtest-pkg-pybuild

Package: python3-prettytable
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Description: library to represent tabular data in visually appealing ASCII tables (Python3)
 PrettyTable is a simple Python library designed to make it quick and
 easy to represent tabular data in visually appealing ASCII tables. It
 was inspired by the ASCII tables used in the PostgreSQL shell
 psql. PrettyTable allows for selection of which columns are to be
 printed, independent alignment of columns (left or right justified or
 centred) and printing of "sub-tables" by specifying a row range.
 .
 This package contains the Python 3 version of prettytable.
